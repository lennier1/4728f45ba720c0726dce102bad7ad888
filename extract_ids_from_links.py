import sys

idDict = {}
allCountryCodes = set()
with open("appStoreLinks.txt") as linkFile:
    for text in linkFile:
        #print(text)
        
        splitText = text.split('/')
        countryCode = splitText[3]
        allCountryCodes.add(countryCode)
        slug = splitText[5]
        id = splitText[6][2:len(splitText[6])]
        id = id.strip()
        
       
        #print(countryCode)        
        #print(slug)
        #print(id)
        
        
        
        if id in idDict:
            idDict[id].append(countryCode)
        else:
            idDict[id] = [countryCode]

#countByCountryDict = {}
#for countryCode in allCountryCodes:
#    countByCountryDict[countryCode] = 0
    
#for key in idDict:
#    for countryCode in idDict[key]:
#        countByCountryDict[countryCode] = countByCountryDict[countryCode] + 1
        
#for key in countByCountryDict:
#    print(key + ": " + str(countByCountryDict[key]))       

#sys.exit()

#print(idDict)

with open("krIds.txt","w") as countrySpecificFile:
    for key in idDict:
        if "kr" in idDict[key] and "us" not in idDict[key] and "jp" not in idDict[key] and "cn" not in idDict[key] and "tw" not in idDict[key]:
            countrySpecificFile.write(key + '\n')